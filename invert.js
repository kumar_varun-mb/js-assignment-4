function invert(obj) {
    let revObj={};
    for(let property in obj){
        revObj[obj[property]]=property;
    }
    return revObj;
}
module.exports = invert;