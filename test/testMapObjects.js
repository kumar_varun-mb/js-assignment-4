const mapObject=require('../mapObject.js');

const testObject = { one: 1, two: 2, three: 3 };

function cb(key, val){
    return val*10;
}

console.log(mapObject(testObject, cb));

