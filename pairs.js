function pairs(obj) {
    let arr=[];
    for(let property in obj){
        arr.push([property, obj[property]]);
    }
    return arr;
}
module.exports = pairs;