function mapObject(obj, cb) {
    let newObj={};
        for(let property in obj){
            newObj[property]=cb(property,obj[property]);
        }
        return newObj;
    }
    module.exports = mapObject;

