function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    let arr=[];
    for(let property in obj){
        arr.push(property);
    }
    return arr;
}
module.exports = keys;